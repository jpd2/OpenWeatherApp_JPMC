package com.jean.myweatherapp.repository

import com.jean.myweatherapp.model.weather.WeatherModel
import com.jean.myweatherapp.util.Resource
import kotlinx.coroutines.flow.Flow

interface Repository {

    fun getWeatherByCity(cityName: String) : Flow<Resource<WeatherModel>>

    fun getWeatherByCoords(latitude: Double?, longitude: Double?) : Flow<Resource<WeatherModel>>
}